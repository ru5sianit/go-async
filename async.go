package goasync

// AsyncTube Объект-хранилище
type AsyncTube struct {
	From chan interface{}
	To   chan interface{}
}

// AsyncTube.New Объявление нового объекта, запуск нового воркера
func (async AsyncTube) New() {
	async.From = make(chan interface{})
	async.To = make(chan interface{})
	go async.NewWorker()
	return
}

//Async.NewBuffered Объявление нового объекта, запуск нового воркера, с буфферизированными каналами
// Требует int значение размера буфера канала
func (async AsyncTube) NewBuffered(buffsize int) {
	async.From = make(chan interface{}, buffsize)
	async.To = make(chan interface{}, buffsize)
	go async.NewWorker()
	return
}

// AsyncTube.NewWorker Создание нового воркера
func (async AsyncTube) NewWorker() {
	var simpleBuff []interface{}

	for {
		lenBuff := len(simpleBuff)
		if lenBuff == 0 {
			select {
			case item := <-async.From:
				select {
				case async.To <- item:
					continue

				default:
					simpleBuff = append(simpleBuff, item)
				}

			}

		} else {
			select {
			case item := <-async.From:
				simpleBuff = append(simpleBuff, item)

			case async.To <- simpleBuff[lenBuff-1]:
				simpleBuff = simpleBuff[:lenBuff-1]
			}

		}

	}
}

//Слабая имплементация асинхронного враппера для каналов
